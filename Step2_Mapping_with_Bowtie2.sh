#!/bin/sh

## Bowtie2 mapping. 
## mkdir "Data" directory and copy raw data fastq files in there. 

for sample in `ls ./Data/*R1.fastq.gz`; do dir="./Data"; base=$(basename $sample "_R1.fastq"); bowtie2 q --phred33 -x iHT115 -1 ${dir}/${base}_R1.fastq -2 ${dir}/${base}_R2.fastq -S ${dir}/${base}.sam; done

bowtie2 -q --phred33 -x iHT115 -1 {base}_R1.fastq.gz -2 ./{base}_R2.fastq.gz | -S ./Results/{base}.sam 