library(Rsubread)
files <- list.files(path = "./Results/", pattern = ".sam$", full.names = TRUE)

ref <- list.files(path = "./", pattern = "GCA_004354945.1_ASM435494v1_genomic.gff$", full.names = TRUE)

fc <- featureCounts(files = files, annot.ext=ref, GTF.featureType="CDS", GTF.attrType ="gene_id", isGTFAnnotationFile = TRUE, isPairedEnd= TRUE)

write.table(x=data.frame(fc$annotation[,c("inference","Length")],fc$counts,stringsAsFactors=FALSE),file="HTOP_in_HT_gtf.counts")
