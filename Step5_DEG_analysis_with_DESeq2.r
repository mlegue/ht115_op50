library(DESeq2)

countData <- read.table("countsHTOP", header = TRUE, row.names = 1)
colData<- read.table("HTOP_conditions", header = TRUE, row.names = 1)
dds <- DESeqDataSetFromMatrix(countData = countData, colData = colData, design = ~ condition)
dds <- DESeq(dds)
result_HTOP_NCBI <- results(dds, contrast=c("condition","HT115","OP50"))
write.table(result_HTOP_NCBI, "DEG_HTOP_NCBI.tbl")