# README #

### What is this repository for? ###

* This repository is to share code for the comparative genomic and transcriptomic analysis of two strains of E. coli (OP50 and HT115)
* Version 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Scripts are organized in five independent steps to be run in bash or R:
	
	Step1_Reciprocal_Best_Hits.sh 
	Step2_Mapping_with_Bowtie2 
	Step3_Counting_features_with_featureCount.r	in R
	Step4_Selecting_common_sequences.awk		
	Step5_DEG_analysis_with_DESeq2 in R

* Configuration 
* Dependencies

in R:
Library Rsubread
library DESeq2
* Database configuration

* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* marcelegue@gmail.com
* Other community or team contact