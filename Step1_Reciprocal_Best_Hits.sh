#!/bin/sh

# Reciprocal Best Blast Hit commands

export PATH=$PATH:$HOME/ncbi-blast-2.9.0+/bin

 mkdir $HOME/blastdb

 makeblastdb -in GCA_004355015.1_ASM435501v1_protein.faa -dbtype 'prot' -out OP50protDb

 blastp -query GCA_004354945.1_ASM435494v1_protein.faa -db OP50protDb -out NCBI_HT50_blastp_OP50Db_fw -outfmt 7

 cat NCBI_HT50_blastp_OP50Db_fw |awk '/hits found/{getline;print}' | grep -v "#" > top_hits_fw.txt

 makeblastdb -in GCA_004354945.1_ASM435494v1_protein.faa -dbtype 'prot' -out HT115protDb

awk '{ print $1}' top_hits_fw.txt > top_hits_fw.list
# awk 'FNR==NR {[$0]; next} $1' ~/top_hits_fw.txt GCA_004354945.1_ASM435494v1_protein.faa > HT115_top_NCBI.faa
grep -w -A 1 -Ff top_hits_fw.list GCA_004354945.1_ASM435494v1_protein.faa --no-group-separator > HT115_top_NCBI.faa

 makeblastdb -in GCA_004354945.1_ASM435494v1_protein.faa -dbtype 'prot' -out HT115protDb

 blastp -query GCA_004355015.1_ASM435501v1_protein.faa -db HT115protDb -out NCBI_OP50_blastp_HT115Db_re -outfmt 7

 cat NCBI_OP50_blastp_HT115Db_re |awk '/hits found/{getline;print}' | grep -v "#" > top_hits_re.txt

 awk 'NR==FNR{a[$1]=$2;next} $2 in a {print $0,a[$2]}' top_hits_re.txt top_hits_fw.txt >top_hits_merged_first

 awk 'NR==FNR{$1=$13; print $0}' top_hits_merged_first > top_hits_filtered

 awk {print $1} top_hits_filtered > HTOP_common_NCBI.list 

 awk 'NR==FNR{print $1}' top_hits_filtered > HTOP_common_NCBI.list #### accession number de secuencias comunes

 awk '!a[$0]++' HTOP_common_NCBI.list > HTOP_common_NCBI.names #### sin redundancia

 grep -A 1 -wFf HTOP_common_NCBI.names GCA_004354945.1_ASM435494v1_protein.faa > HT115_common.fa

 grep -vf HTOP_common_NCBI.names HT115_accession_all.list > HT115_unique.list ### lista secuencias unicas HT115

 grep -A 1 -wFf HT115_unique.list GCA_004354945.1_ASM435494v1_protein.faa > HT115_unique.fa ### fasta file de secuencias únicas